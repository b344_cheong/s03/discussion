package com.zuitt.example;

import java.util.Arrays;

public class RepetitionControl {
    public static void main(String[] args) {
        // [Section] Loops
        // Are control structures that allow code blocks  to be executed multiple times.

        // While Loop
        // Allows for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate is infinite.

        // To create the variable that will serve as the basis of our condition.
        System.out.println("Displaying While Loop: ");
        int x = 0;
        while ( x < 10 ){
            System.out.println("The current value of x is : " + x);
            x++;
        }

        // Do-While Loop
        // Similar to While Loop. However, Do-While loops always execute at least once - while loops may not execute/run at all.
        int y = 11;
        do {
            System.out.println("The current value of y is : " + y);
            y++;
        } while (y < 10);

        // For Loop
        // Syntax:
        // for (initialVal; condition/stopper; iteration{code-block/statement})
        for (int i = 0; i < 10; i++) {
            System.out.println("The current value of i is : " + i);
        }

        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        int[] numArray = {2, 3, 5, 7, 11};

        System.out.print("\nLooping nameArray: ");
        // For Loop for arrays:
        for (String name : nameArray){
            System.out.print(name + " ");
        }
        System.out.print("\n\nLooping numArray: ");
        for (int prime : numArray){
            System.out.print(prime + " ");
        }

        // Mini Activity
        String[][] classrooms = new String[3][3];

        classrooms[0][0] = "Athos";
        classrooms[0][1] = "Porthos";
        classrooms[0][2] = "Aramis";

        classrooms[1][0] = "Brandon";
        classrooms[1][1] = "JunJun";
        classrooms[1][2] = "Robert";

        classrooms[2][0] = "Mickey";
        classrooms[2][1] = "Donald";
        classrooms[2][2] = "Goofy";

        System.out.print("\n\nLooping classroom student names: ");
        /* One Solution
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(classrooms[i][j] + " ");
            }
        } */
        // Another Solution
        for (String[] row : classrooms){
            // Add another for loop that will iterate on the nested array inside our classrooms array
            // System.out.println(Arrays.toString(row));
            // [Athos, Porthos, Aramis], [Brandon, JunJun, Robert], [Mickey, Donald, Goofy]
            for (String column : row) {
                // Athos, Porthos, Aramis Brandon, JunJun, Robert, Mickey, Donald, Goofy
                System.out.print(column + " ");
            }
        }
    }
}
